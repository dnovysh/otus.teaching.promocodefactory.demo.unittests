﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions
{
    public class ChangePartnerLimitException
        : Exception
    {
        public ChangePartnerLimitException()
        {
            
        }
        
        public ChangePartnerLimitException(string message)
            : base(message)
        {
            
        }
        
        public ChangePartnerLimitException(string message, Exception exception)
            : base(message, exception)
        {
            
        }
    }
}